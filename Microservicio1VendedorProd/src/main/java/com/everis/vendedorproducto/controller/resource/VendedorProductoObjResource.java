package com.everis.vendedorproducto.controller.resource;

import java.math.BigDecimal;

import com.everis.vendedorproducto.model.entity.Producto;
import com.everis.vendedorproducto.model.entity.Vendedor;

import lombok.Data;

@Data
public class VendedorProductoObjResource {

	private Long id;
	private Producto idProducto;
	private Vendedor idVendedor;
	private BigDecimal precio;
}
