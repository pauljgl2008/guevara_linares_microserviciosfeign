package com.everis.vendedorproducto.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data

public class VendedorProductoReducidoResource {
	private Long id;
	private Long idProducto;
	private Long idVendedor;
	private BigDecimal precio;
}