package com.everis.vendedorproducto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.vendedorproducto.controller.resource.VendedorProductoObjResource;
import com.everis.vendedorproducto.controller.resource.VendedorProductoReducidoResource;
import com.everis.vendedorproducto.controller.resource.VendedorProductoResource;
import com.everis.vendedorproducto.controller.resource.VendedorReducidoResource;
import com.everis.vendedorproducto.controller.resource.VendedorResource;
import com.everis.vendedorproducto.dao.ProductoClient;
import com.everis.vendedorproducto.model.entity.Producto;
import com.everis.vendedorproducto.model.entity.Vendedor;
import com.everis.vendedorproducto.model.entity.VendedorProducto;
import com.everis.vendedorproducto.service.VendedorProductoService;
import com.everis.vendedorproducto.service.VendedorService;

@RestController

public class ApiController {
	@Autowired
	VendedorService vendedorService;
	
	@Autowired
	VendedorProductoService vendedorProductoService;
	
	@Autowired
	ProductoClient productoClient;
	
	@PostMapping("/vendedorProducto")
	public VendedorProductoResource guardarVendedorProducto(@RequestBody VendedorProductoReducidoResource request) throws Exception {
				
		Producto producto = productoClient.obtenerProductoPorId(request.getIdProducto());
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(request.getIdVendedor());

		VendedorProducto nuevo = new VendedorProducto();
		nuevo.setIdProducto(producto);
		nuevo.setIdVendedor(vendedor);
		nuevo.setPrecio(request.getPrecio());
		VendedorProducto vendedorProducto = vendedorProductoService.insertar(nuevo);
		
		VendedorProductoResource vendedorProductoResource = new VendedorProductoResource();
		vendedorProductoResource.setId(vendedorProducto.getId());
		vendedorProductoResource.setIdProducto(vendedorProducto.getIdProducto().getId());
		vendedorProductoResource.setIdVendedor(vendedorProducto.getIdVendedor().getId());
		vendedorProductoResource.setPrecio(vendedorProducto.getPrecio());

		return vendedorProductoResource; 
	}

	
	@PostMapping("/vendedor")
	public VendedorResource guardarVendedor(@RequestBody VendedorReducidoResource request) throws Exception {
				
		Vendedor nuevo = new Vendedor();
		nuevo.setNombres(request.getNombres());
		nuevo.setApellidos(request.getApellidos());
		nuevo.setCargo(request.getCargo());
		Vendedor vendedor = vendedorService.insertar(nuevo);
		
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setId(vendedor.getId());
		vendedorResource.setNombres(vendedor.getNombres());
		vendedorResource.setApellidos(vendedor.getApellidos());
		vendedorResource.setCargo(vendedor.getCargo());

		return vendedorResource; 
	}
	
	@GetMapping("/vendedores/{id}")
	public VendedorResource obtenerVendedorPorId(@PathVariable("id") Long id) throws Exception {
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(id);
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setId(vendedor.getId());
		vendedorResource.setNombres(vendedor.getNombres());
		vendedorResource.setApellidos(vendedor.getApellidos());
		vendedorResource.setCargo(vendedor.getCargo());

		return vendedorResource;
	}
	@GetMapping("/vendedorProductos/{id}")
	public VendedorProductoObjResource obtenerVendedorProductoPorId(@PathVariable("id") Long id) throws Exception {
		VendedorProducto vendedorProducto = vendedorProductoService.obtenerVendedorProductoPorId(id);
		VendedorProductoObjResource vendedorProductoResource = new VendedorProductoObjResource();
		vendedorProductoResource.setId(vendedorProducto.getId());
		vendedorProductoResource.setPrecio(vendedorProducto.getPrecio());
		vendedorProductoResource.setIdProducto(vendedorProducto.getIdProducto());
		vendedorProductoResource.setIdVendedor(vendedorProducto.getIdVendedor());

		return vendedorProductoResource;
	}
	@GetMapping("/vendedores")
	public List<VendedorResource> obtenerVendedores(){
		
		List<VendedorResource> listado = new ArrayList<>();
		
		vendedorService.obtenerVendedores().forEach(vendedor -> {
			VendedorResource vendedorResource = new VendedorResource();
			vendedorResource.setId(vendedor.getId());
			vendedorResource.setNombres(vendedor.getNombres());
			vendedorResource.setApellidos(vendedor.getApellidos());
			vendedorResource.setCargo(vendedor.getCargo());
			listado.add(vendedorResource);
		});
		
		return listado;			
	}
	
/*	@GetMapping("/productos2")
	public List<VendedorResource> obtenerProductos(){
		
		List<VendedorResource> listado = new ArrayList<>();
		
		vendedorService.obtenerProductos().forEach(vendedor -> {
			VendedorResource vendedorResource = new VendedorResource();
			vendedorResource.setId(vendedor.getId());
			vendedorResource.setNombres(vendedor.getNombre());
			vendedorResource.setApellidos(vendedor.getDescripcion());
			vendedorResource.setCargo(vendedor.getUnidadMedida());
			listado.add(vendedorResource);
		});
		
		return listado;			
	}*/
}
