package com.everis.vendedorproducto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.everis.vendedorproducto.model.entity.VendedorProducto;
import com.everis.vendedorproducto.model.repository.VendedorProductoRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class VendedorProductoServiceImpl implements VendedorProductoService{

	@Autowired
	private VendedorProductoRepository vendedorProductoRepository;
	
	@Override
	public VendedorProducto insertar(VendedorProducto vendedorProducto) throws Exception {
		return vendedorProductoRepository.save(vendedorProducto);
	}
	
	@Override
	public VendedorProducto obtenerVendedorProductoPorId(Long id) throws Exception {
		return vendedorProductoRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"VendedorProducto no encontrado"));
	}
}
