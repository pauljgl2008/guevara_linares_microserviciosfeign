package com.everis.vendedorproducto.dao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.everis.vendedorproducto.model.entity.Producto;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient("MICRO3PRODUCTO")
public interface ProductoClient {

	//@RequestMapping(value="/productos", method=RequestMethod.GET)
	//Iterable<Producto> getProductos();
	@GetMapping("/productos/{id}")
	Producto obtenerProductoPorId(@PathVariable("id") Long id) throws Exception;
}
