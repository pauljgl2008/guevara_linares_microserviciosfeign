package com.everis.ordendetalle.service;

import com.everis.ordendetalle.model.entity.DetalleOrden;

public interface  DetalleOrdenService {
			
	public DetalleOrden insertar(DetalleOrden detalleOrden) throws Exception;
	
}
