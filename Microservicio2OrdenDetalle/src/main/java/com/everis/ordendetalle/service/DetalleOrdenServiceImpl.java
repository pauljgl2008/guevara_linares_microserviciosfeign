package com.everis.ordendetalle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.ordendetalle.model.entity.DetalleOrden;
import com.everis.ordendetalle.model.repository.DetalleOrdenRepository;

@Service
public class DetalleOrdenServiceImpl implements DetalleOrdenService{
	
	@Autowired
	private DetalleOrdenRepository detalleOrdenRepository;
	
	@Override
	public DetalleOrden insertar(DetalleOrden detalleOrden) throws Exception {
		return detalleOrdenRepository.save(detalleOrden);
	}
	
}
