package com.everis.ordendetalle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.ordendetalle.model.entity.Orden;
import com.everis.ordendetalle.model.repository.OrdenRepository;

@Service
public class OrdenServiceImpl implements OrdenService{
	
	@Autowired
	private OrdenRepository ordenRepository;
	
	@Override
	public Orden insertar(Orden orden) throws Exception {
		return ordenRepository.save(orden);
	}
	
}
