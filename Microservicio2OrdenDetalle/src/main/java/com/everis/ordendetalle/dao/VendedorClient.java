package com.everis.ordendetalle.dao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.everis.ordendetalle.model.entity.Vendedor;
import com.everis.ordendetalle.model.entity.VendedorProducto;

import org.springframework.cloud.openfeign.FeignClient;


@FeignClient("MICRO1VENDEDORPROD")
public interface VendedorClient {

	@GetMapping("/vendedores/{id}")
	Vendedor obtenerVendedorPorId(@PathVariable("id") Long id) throws Exception;
	
	@GetMapping("/vendedorProductos/{id}")
	VendedorProducto obtenerVendedorProductoPorId(@PathVariable("id") Long id) throws Exception;
}
