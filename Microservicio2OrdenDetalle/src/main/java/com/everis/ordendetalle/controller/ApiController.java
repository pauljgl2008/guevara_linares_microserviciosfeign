package com.everis.ordendetalle.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.ordendetalle.controller.resource.OrdenReducidoResource;
import com.everis.ordendetalle.controller.resource.OrdenRespResource;
import com.everis.ordendetalle.dao.ClienteClient;
import com.everis.ordendetalle.dao.VendedorClient;
import com.everis.ordendetalle.model.entity.Cliente;
import com.everis.ordendetalle.model.entity.DetalleOrden;
import com.everis.ordendetalle.model.entity.Orden;
import com.everis.ordendetalle.model.entity.VendedorProducto;
import com.everis.ordendetalle.service.DetalleOrdenService;
import com.everis.ordendetalle.service.OrdenService;

@RestController

public class ApiController {
	@Autowired
	OrdenService ordenService;
	
	@Autowired
	DetalleOrdenService detalleOrdenService;
	
	@Autowired
	ClienteClient clienteClient;
	
	@Autowired
	VendedorClient vendedorClient;
	
	@Value("${igv}")
	BigDecimal igv;
	
	@GetMapping("/igv")
	public BigDecimal igv(){
		return igv;
	}
	
	@PostMapping("/orden")
	public OrdenRespResource guardarOrden(@RequestBody OrdenReducidoResource request) throws Exception {
				
		Cliente cliente = clienteClient.obtenerClientePorId(request.getIdCliente());
		VendedorProducto vendedorProducto = vendedorClient.obtenerVendedorProductoPorId(request.getIdVendedorProducto());

		Orden nuevaOrden = new Orden();
		nuevaOrden.setIdCliente(cliente);
		nuevaOrden.setIdVendedor(vendedorProducto.getIdVendedor());
		BigDecimal cantidad = new BigDecimal(request.getCantidad());
		BigDecimal igvMonto, total;
		total = cantidad.multiply(vendedorProducto.getPrecio()).setScale(2, RoundingMode.FLOOR);
		igvMonto = igv.multiply(total).setScale(2, RoundingMode.FLOOR);
		total = total.add(igvMonto);
		nuevaOrden.setTotal(total);
		nuevaOrden.setIgv(igvMonto);
		    
		
		Orden orden = ordenService.insertar(nuevaOrden);
		
		DetalleOrden nuevoDetalle = new DetalleOrden();
		nuevoDetalle.setIdOrden(orden);
		nuevoDetalle.setIdVendedorProducto(vendedorProducto);
		nuevoDetalle.setPrecioUnitario(vendedorProducto.getPrecio());
		nuevoDetalle.setCantidad(request.getCantidad());
		
		DetalleOrden detalle = detalleOrdenService.insertar(nuevoDetalle);
		
		OrdenRespResource ordenRespResource = new OrdenRespResource();
		ordenRespResource.setOrden(orden);
		ordenRespResource.setDetalleOrden(detalle);
		return ordenRespResource; 
	}
}
