package com.everis.ordendetalle.controller.resource;

import lombok.Data;

@Data
public class OrdenReducidoResource {

	private Long idCliente;
	private Long idVendedorProducto;
	private Integer cantidad;

}
