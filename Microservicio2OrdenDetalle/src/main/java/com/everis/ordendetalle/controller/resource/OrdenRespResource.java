package com.everis.ordendetalle.controller.resource;

import com.everis.ordendetalle.model.entity.DetalleOrden;
import com.everis.ordendetalle.model.entity.Orden;

import lombok.Data;

@Data
public class OrdenRespResource {

	private Orden orden;
	private DetalleOrden detalleOrden;

}
