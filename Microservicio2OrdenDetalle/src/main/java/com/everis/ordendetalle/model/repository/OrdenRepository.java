package com.everis.ordendetalle.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.ordendetalle.model.entity.Orden;

@Repository
public interface OrdenRepository extends CrudRepository<Orden, Long> {

}
