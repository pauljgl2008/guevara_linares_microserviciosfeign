package com.everis.ordendetalle.model.entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Orden {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	private Cliente idCliente;
	@ManyToOne
	private Vendedor idVendedor;
	@Column
	private String fechaOrden = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	@Column
	private BigDecimal total;
	@Column
	private BigDecimal igv;

}
