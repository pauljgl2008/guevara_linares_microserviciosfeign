package com.everis.ordendetalle.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.ordendetalle.model.entity.DetalleOrden;


@Repository
public interface DetalleOrdenRepository extends CrudRepository<DetalleOrden, Long> {

}
