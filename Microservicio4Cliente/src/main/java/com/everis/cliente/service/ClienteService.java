package com.everis.cliente.service;

import com.everis.cliente.model.entity.Cliente;

public interface  ClienteService {
		
	public Iterable<Cliente> obtenerClientes();
	
	public Cliente insertar(Cliente cliente) throws Exception;;
	
	public Cliente obtenerClientePorId(Long id) throws Exception;

}
