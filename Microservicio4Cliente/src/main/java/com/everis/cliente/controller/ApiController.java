package com.everis.cliente.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.cliente.controller.resource.ClienteReducidoResource;
import com.everis.cliente.controller.resource.ClienteResource;
import com.everis.cliente.model.entity.Cliente;
import com.everis.cliente.service.ClienteService;

@RestController

public class ApiController {
	@Autowired
	ClienteService clienteService;
	
	@PostMapping("/cliente")
	public ResponseEntity<ClienteResource> guardarCliente(@RequestBody ClienteReducidoResource request) throws Exception {
				
		Cliente cliente = new ModelMapper().map(request, Cliente.class);
		Cliente clienteNuevo = clienteService.insertar(cliente);
		ClienteResource clienteResource = new ModelMapper().map(clienteNuevo, ClienteResource.class);
		
		return new ResponseEntity<>(clienteResource, HttpStatus.CREATED); 
	}
	
	@GetMapping("/clientes/{id}")
	public ClienteResource obtenerProductoPorId(@PathVariable("id") Long id) throws Exception {
		Cliente cliente = clienteService.obtenerClientePorId(id);
		ModelMapper modelMapper = new ModelMapper();
		ClienteResource clienteResource = modelMapper.map(cliente, ClienteResource.class);

		return clienteResource;
	}
	
	@GetMapping("/clientes")
	public List<ClienteResource> getClientes(){
		
		List<ClienteResource> listado = new ArrayList<>();
		
		clienteService.obtenerClientes().forEach(cliente -> {
			ModelMapper modelMapper = new ModelMapper();
			ClienteResource clienteResource = modelMapper.map(cliente, ClienteResource.class);
			listado.add(clienteResource);
		});
		
		return listado;			
	}
}
