package com.everis.producto.controller.resource;

import lombok.Data;

@Data

public class ProductoReducidoResource {
	private String nombre;
	private String descripcion;
	private String unidadMedida;// -> primero buscar el código del tipo de producto y almacenarlo
}