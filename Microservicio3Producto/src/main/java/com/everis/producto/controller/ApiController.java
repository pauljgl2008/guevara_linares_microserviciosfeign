package com.everis.producto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.producto.controller.resource.ProductoReducidoResource;
import com.everis.producto.controller.resource.ProductoResource;
import com.everis.producto.model.entity.Producto;
import com.everis.producto.service.ProductoService;


@RestController

public class ApiController {
	@Autowired
	ProductoService productoService;
	
	@PostMapping("/producto")
	public ProductoResource guardarProducto(@RequestBody ProductoReducidoResource request) throws Exception {
				
		Producto nuevo = new Producto();
		nuevo.setNombre(request.getNombre());
		nuevo.setDescripcion(request.getDescripcion());
		nuevo.setUnidadMedida(request.getUnidadMedida());
		Producto producto = productoService.insertar(nuevo);
		
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setUnidadMedida(producto.getUnidadMedida());

		return productoResource; 
	}
	
	@GetMapping("/productos/{id}")
	public ProductoResource obtenerProductoPorId(@PathVariable("id") Long id) throws Exception {
		Producto producto = productoService.obtenerProductoPorId(id);
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setUnidadMedida(producto.getUnidadMedida());

		return productoResource;
	}
	
	@GetMapping("/productos")
	public List<ProductoResource> obtener2(){
		
		List<ProductoResource> listado = new ArrayList<>();
		
		productoService.obtenerProductos().forEach(producto -> {
			ProductoResource productoResource = new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setNombre(producto.getNombre());
			productoResource.setDescripcion(producto.getDescripcion());
			productoResource.setUnidadMedida(producto.getUnidadMedida());
			listado.add(productoResource);
		});
		
		return listado;			
	}
}
